let _writer = require('./_writer');
let _bars = require('./_bars');
let Crawler = require('crawler');

let crawler = new Crawler({
    maxConnections : 10,
});

module.exports = {
  _audios: [],
  img: null,
  title: null,
  pages: 1,
  multibar: null,
  author: null,
  readPage: function(res, ptoken, opdir, reportProgress, endProgress){
      let $ = res.$;
      let _self = this;

      if(ptoken === 1) {
          _self.img = $('article figure img').attr('data-src');
      }

      let ind = 0;

      $('audio source').each(function(){
          let targetUrl = $(this).attr('src');
          let index = ind;

          if(ptoken > 1){
              index = (ptoken - 1) * 7 + ind - 1;
          }
          _self._audios[index] = targetUrl;

          ind++;
      });

      if(ptoken === 1){
          _self.downloadMorePages($, opdir, reportProgress, endProgress)
      }

      if(ptoken === _self.pages){
          _self.downloadAudio(opdir, reportProgress, endProgress);
      }
  },
  downloadMorePages: function($, opdir, reportProgress, endProgress){
      let _self = this;
      let it = 2;
      $('.page-links a').each(function () {
          crawler.queue({
              uri: $(this).attr('href'),
              callback : function (errorx, resx, donex) {
                  if(errorx){
                      console.log(errorx);
                  }else{
                      _self.readPage(resx, it, opdir, reportProgress, endProgress);
                      it++;
                  }
                  donex();
              }
          });
          _self.pages++;
      });
  },
  handleUrl: function(txt){
    return txt.split('?')[0];
  },
  downloadAudio: function(opdir, reportProgress, endProgress) {
    let _self = this;
    let ix = 0;
    let imgData = _self.img ? _self.handleUrl(_self.img) : null;

    _self._audios.forEach(function (targetUrl) {
        _writer
            .writeFile(_self.multibar, _self.handleUrl(targetUrl), ix, imgData, _self.title, _self.author, opdir,reportProgress, endProgress)
            .then(function (val) {

            })
            .catch((reason) => {
                console.log('Handle rejected promise (' + reason + ') here.');
            });
        ix++;
    });
  },
  download: function(url, ptoken, title, author, opdir, operator, reportProgress, endProgress){
      let _self = this;
      this.title = title;
      this.author = author;
      this.multibar = operator;

      crawler.queue({
          uri: url,
          callback : function (error, res, done) {
              if(error){
                  console.log(error);
              }else{
                  _self.readPage(res, ptoken, opdir, reportProgress, endProgress);
              }
              done();
          }
      });
  }
};