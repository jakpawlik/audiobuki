module.exports = class FileTree {
    constructor(path, fs, name = null){
        this.path = path;
        this.name = name;
        this.fs = fs;
        this.items = [];
    };

    build = () => {
        this.items = FileTree.readDir(this.path, this.fs);
    };

    

    static readDir(path, electronFs) {
        var fileArray = [];

        electronFs.readdirSync(path).forEach(file => {
            var fileInfo = new FileTree(`${path}\\${file}`, file);

            var stat = electronFs.statSync(fileInfo.path);

            if (stat.isDirectory()){
                fileInfo.items = FileTree.readDir(fileInfo.path);
            }

            fileArray.push(fileInfo);
        });

        return fileArray;
    }
};