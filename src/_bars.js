const cliProgress = require('cli-progress');

module.exports = {
    _bars: [],
    _doneJobs: [],
    stopBar: function(i, txt){
        this._bars[i].stop();
        this._doneJobs.push(txt);
    },
    goBar: function(multibar, cur, percent, len, fileName, i){
        if(!!this._bars[i]) {
            this._bars[i].update(cur, {filename: fileName, percent: percent});
        }else{
            this._bars[i] = multibar.create(len, cur);
        }
    },
    prepareBars: function () {
        let $self = this;

        return new cliProgress.MultiBar({
            format: '{percentage}% | {bar} | {value}MB / {total}MB | "{filename}"',
            hideCursor: true,
            barCompleteChar: '\u2588',
            barIncompleteChar: '\u2591',
            clearOnComplete: true,
            stopOnComplete: true
        }, cliProgress.Presets.rect).on('end', () => {
            $self._doneJobs.forEach(function (e) {
                console.log(e);
            });
        });
    }
};