const MB_SIZE = 1048576;

let https = require('https');
const NodeID3 = require('node-id3');
let fs = require('fs');
let path = require('path');
let dotenv = require('dotenv');
let config = dotenv.config();

module.exports = {
    tag: function(title, author, albumimg, file, fileName, i){
        let tags = {
            title: title,
            artist: author,
            album: title,
            APIC: albumimg,
            originalFilename: fileName,
            TRCK: i
        };

        let ID3FrameBuffer = NodeID3.create(tags);   //  Returns ID3-Frame buffer
        //  Asynchronous
        NodeID3.create(tags, function(frame) {  });

        //  Write ID3-Frame into (.mp3) file
        let success = NodeID3.write(tags, file); //  Returns true/false or, if buffer passed as file, the tagged buffer
        NodeID3.write(tags, file, function(err, buffer) {  }); //  Buffer is only returned if a buffer was passed as file

        console.log('Tagged ' + fileName);
    },
    writeAlbum: function(albumPath, img){
        if (!fs.existsSync(albumPath)){
            const albumFile = fs.createWriteStream(albumPath);
            const albumRequest = https.get(img, function(aresponse) {
                aresponse.pipe(albumFile);
                console.log('Downloaded album cover from: ' + img);
            });
        }
    },
    writeAudio: function(multibar, filePath, url, fileName, i, title, author, resolve, reportProgress, endProgress){
        let $this = this;

        console.log(filePath);

        const file = fs.createWriteStream(filePath);
        const request = https.get(url, function(response) {
            let cur = 0;
            let len = parseInt(response.headers['content-length'], 10);
            let total = len / MB_SIZE;

            response.on("data", function(chunk) {
                cur += chunk.length;
                let percent =  100.0 * (cur / MB_SIZE) / total;
                reportProgress(multibar, Math.round(cur / MB_SIZE, 2), percent, Math.round(total,2), fileName, i);
            });

            response.on("end", function() {
                reportProgress(multibar, Math.round(total,2), 100, Math.round(total,2), fileName, i);
                endProgress(i, 'Downloaded ' + fileName + '(' + total.toFixed(2) + ' MB)');

                $this.tag(title, author, file, fileName, i);
                resolve(title, author, file, fileName, i);
            });

            request.on("error", function(e){
                console.log("Error: " + e.message);
            });

            response.pipe(file);
        });
    },
    writeFile: function(multibar, url, i, img, title, author, opdir, reportProgress, endProgress){
        let _self = this;
        return new Promise(function(resolve, reject){
            let ext = url.split('.');
            ext = ext[ext.length - 1];

            let OPERATE_DIR = opdir ? opdir : config.parsed.AUDIO_DIR;


            let fileName = (i + 1) + '-' + author.replace(' ', '-') + '-' + title.replace(' ', '-') + '.' + ext;
            let targetPath = author + path.sep + title;
            let basePath = targetPath + path.sep + fileName;
            let filePath =  OPERATE_DIR + path.sep + basePath;
            let checkPath = OPERATE_DIR;

            targetPath.split(path.sep).forEach(function(element) {
                checkPath = checkPath + path.sep + element;

                let isFolder = element.indexOf('.') === -1;
                if (isFolder && !fs.existsSync(checkPath)){
                    fs.mkdirSync(checkPath);
                }
            });

            if(img) {
                let imgExt = img.split('.');
                imgExt = imgExt[imgExt.length - 1];

                let albumPath = OPERATE_DIR + path.sep + targetPath + path.sep + 'album.' + imgExt;
                _self.writeAlbum(albumPath, img);
            }

            _self.writeAudio(multibar, filePath, url, fileName, i, title, author, resolve, reportProgress, endProgress);
        });
    }
};