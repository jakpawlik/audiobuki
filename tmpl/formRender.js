let downloader = require('../src/_downloader');
const ipcRenderer = require('electron').ipcRenderer;
let dotenv = require('dotenv');
let path = require('path');
let config = dotenv.config();

const { shell } = require('electron');

document.querySelectorAll('.search').forEach(function (el) {
    el.addEventListener('click', (e) => {
        e.preventDefault();
        shell.openExternal(el.href);
    });
});

document.getElementById('path').addEventListener('click', (e) => {
    e.preventDefault();
    ipcRenderer.send('path-click', document);
});

document.getElementById('path_val').addEventListener('change', () => {
    // window.postMessage({
    //     type: 'select-dir'
    // })

    let fpath = document.getElementById('path_label');

    fpath.innerText = document.getElementById("path_val").value;
});

document.addEventListener('DOMContentLoaded',pageLoaded);

function pageLoaded(){
    document.getElementById('path_label').innerText = config.parsed.AUDIO_DIR;
}

function sendForm(event) {
    event.preventDefault();   // stop the form from submitting
    let path_dir = document.getElementById("path_val").value ? document.getElementById("path_val").value : null;

    if(!path_dir){
        path_dir = config.parsed.AUDIO_DIR;
    }

    let uri = document.getElementById("uri").value;
    let title = document.getElementById("title").value;
    let author = document.getElementById("author").value;

    ipcRenderer.send('form-submission', uri, title, author);

    let progress = {
        operator: {
            init: function () {

            }
        },
        goBar: function(operator, current, percent, max, fileName, i){
            let elId= 'progress_list_el_' + i;
            let barsWrap = document.getElementById('progress_list');
            let elWrap = document.getElementById(elId);

            let currentText = current.toFixed(2) + 'MB / ' + max.toFixed(2) + 'MB ('+ Math.round(percent) +'%)';

            let elHtml = '<div class="wrap-bar-contents"><div class="progress-file">'+fileName+'</div>\n' +
                '        <div class="progress-bar">\n' +
                '            <div class="bar positive" style="width: ' + (percent) +'%;">\n' +
                '                <span>'+ currentText +'</span>\n' +
                '            </div>\n' +
                '            <div class="bar negative" style="width:'+ (100 - percent) +'%;">\n' +
                '                <span>'+ currentText +'</span>\n' +
                '            </div>\n' +
                '        </div></div>';

            if(elWrap){
                elWrap.innerHTML = elHtml;
            }else{
                barsWrap.innerHTML += '<div class="progress-element" id="'+elId+'">'+elHtml+'</div>'
            }
        },
        stopBar: function(i, txt){

        }
    };

    downloader.download(uri, 1, title, author, path_dir, null, function(operator, current, percent, max, fileName, i){
        progress.goBar(operator, current, percent, max, fileName, i);
    }, function(i, txt){
        progress.stopBar(i, txt);
    });
}

ipcRenderer.on('path-chosen', (e, path) => {
    let file_hidden = document.getElementById('path_val');
    let file_label = document.getElementById('path_label');

    file_hidden.value = path;
    file_label.innerHTML = path;
});