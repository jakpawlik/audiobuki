let FileTree = require("./src/_filetree");
let downloader = require('./src/_downloader');

const electron = require('electron');
const { app, BrowserWindow, ipcMain, remote, dialog, fs, session } = require('electron');

// Enable live reload for all the files inside your project directory
// let hardReload = true;
// let reloadOpts = {};
//
// if(hardReload) {
//     reloadOpts = {
//         // Note that the path to electron may vary according to the main file
//         electron: require(`${__dirname}/node_modules/electron`)
//     };
// }

// require('electron-reload')(__dirname, reloadOpts);

let dotenv = require('dotenv');

let config = dotenv.config();

let ENV_DEV = false;

if(config.parsed){
    ENV_DEV = config.parsed.ENV === 'DEV';
}

let window = null;

let handleOpenFolder = () => {
    let options = {
        // See place holder 1 in above image
        title : "Wybierz folder docelowy",

        // See place holder 2 in above image
        defaultPath : "C:\\",

        // See place holder 3 in above image
        buttonLabel : "Wybierz",

        // See place holder 4 in above image
        filters :[
            {name: 'Folders', extensions: ['']},
        ],
        properties: ['openDirectory']
    };

    dialog.showOpenDialog(options).then((data) => {
        let pathValue = data.filePaths[0];
        window.send('path-chosen', pathValue);

        // document.getElementById('path_val').value = data.filePaths[0];
        // console.log(document.getElementById('path_val').value);
    }).catch(function (e) {
        console.log(e.message);
    });
};

function createWindow () {
    let window = new BrowserWindow({
        show: false,
        width: 1200,
        height: 768,
        title: "Black audiobook download",
        frame: true,
        icon: './baha.png',
        webPreferences: {
            nodeIntegration: true
        }
    });

    // window.loadFile('tmpl/index.html');
    window.loadURL(`file://${__dirname}/tmpl/index.html`);

    window.once('ready-to-show', function (){
        window.show();
    });

    if(ENV_DEV) {
        window.webContents.openDevTools();
    }else{
        window.removeMenu();
    }


    let contents = window.webContents;

    window.on('closed', function() {
        window = null;
    });

    return window;
}


ipcMain.on('form-submission', function(targetWindow, uri, title, author) {
    console.log("download uri ->", uri);
});

ipcMain.on('path-click', function(window, document) {
    handleOpenFolder();
});

app.on('ready', function(){
    window = createWindow();
});



